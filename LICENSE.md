[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by/4.0/)

## License and Acknowledgements

**Unless otherwise indicated, this content by the Digital Impact Alliance at United Nations Foundation, collectively known as "DIAL Open Source Center Documentation", is licensed under a Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.**

The work "DIAL Open Source Center Contribution Policy" was adapted from "LibreHealth Contribution Policy" by Software Freedom Conservancy, Inc., used under the Creative Commons 4.0 International Attribution License (CC BY 4.0).

The work “DIAL Open Source Center Developer Certificate of Origin” was adapted from “Developer’s Certificate of Origin 1.1” by The Linux Foundation, Inc., used under the Creative Commons Attribution-ShareAlike 2.5 License (CC BY-SA 2.5).

The work "DIAL Open Source Center Copyright & DMCA Policy” is a derivative of both "LibreHealth Contribution Policy" by Software Freedom Conservancy, Inc., and “Copyright Policy” by the Electronic Frontier Foundation (EFF), both used under the Creative Commons 4.0 International Attribution License (CC BY 4.0).

The work “DIAL Open Source Center Code of Conduct” is adapted from the Contributor Covenant, version 1.4, available at http://contributor-covenant.org/version/1/4

The work "DIAL Open Source Center Privacy Policy" was adapted from "LibreHealth Privacy Policy" by Software Freedom Conservancy, Inc., used under the Creative Commons 4.0 International Attribution License (CC BY 4.0).

The work "DIAL Open Source Center Trademark Policy" was adapted from "Trademark Policy" by the Apache Software Foundation, licensed under the Apache License version 2.0.

The works "DIAL Open Source Center Governance Structure" & “Decision Making Processes” were adapted from "Governance" by Xen Project, used under the Creative Commons Attribution 3.0 License (CC BY 3.0).

The works “Project & Team Lifecycle” &  “DIAL Open Source Center Incubation Advancement Criteria” are derivatives of both “Incubation Policy” by the Apache Software Foundation, licensed under the Apache License 2.0; as well as “Project Incubation Exit Criteria” by The Linux Foundation, used under the Creative Commons 4.0 International Attribution License (CC BY 4.0).