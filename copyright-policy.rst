.. _copyright-policy:

Copyright & DMCA Policy
=======================

All software on any Open Source Center (OSC) website or code repository is distributed to the public by United Nations Foundation, Inc. under the Mozilla Public License 2.0. All other content such as documentation or other creative works is distributed under the Creative Commons Attribution 4.0 International License.

United Nations Foundation, Inc. and the Open Source Center respect all copyrights and copyright licenses. If you see any infringing content on a Open Source Center website, email list or any other resource, please refer to the DMCA instructions at the end of this page.

Software and Documentation Licenses
-----------------------------------

Individual components of Open Source Center-released software and documentation may also be available to the public under the open licenses chosen by their copyright holders.

We disclose information about those components and their licenses in the NOTICE file that accompanies our software.

See the Open Source Center Contribution Policy for more information about the contents of the NOTICE file.

Website Content Copyrights
--------------------------

Any and all original material that is posted by any member of the public or by a registered Open Source Center contributor to any Center-operated website shall be publicly available under the Creative Commons Attribution 4.0 International License, unless otherwise noted. Certain material that is not original to the Open Source Center (which will be specifically noted as such) may require permission from the copyright holder to redistribute.

You do NOT have to ask permission to post your own material on the Open Source Center forum. Refer to the Open Source Center Privacy Policy for additional information about such postings.

You do NOT have to ask permission to reprint an Open Source Center statement from our website in an article. Permission to do this is explicitly granted. Note also that the open source and Creative Commons licenses used for copyrighted materials on Open Source Center websites all allow such fair uses.

If you redistribute something you retrieved from a Open Source Center website, please inform recipients where that copyrighted work originated, so people can get more information or updated versions directly at the Open Source Center website.

OSC Trademarks
--------------

Open Source Center trademarks and logos may only be used in their unmodified forms. They are not licensed under an open source license. You do NOT have to ask permission to use the official Open Source Center logo as a hyperlink to a Open Source Center website from your own website.

Refer to the Open Source Center Trademark Policy for additional information.

DMCA Notices and Copyright Infringement Notification
----------------------------------------------------

If you believe there is content on an Open Source Center website that violates copyright law, let us know. The notice should be sent to our designated agent, Michael Downey via email (osc@digitalimpactalliance.org).

Specifically, send us an email or letter that includes substantially the following:

1. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
2. Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site.
3. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material.
4. Information reasonably sufficient to permit the service provider to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.
5. A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law.
6. A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.

We may display a copy of your DMCA notice in place of the removed content.

*Note: Under Section 512(f) of the DMCA, any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability for damages. In addition, “in order for a copyright owner to proceed under the DMCA with “a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law,” the owner must evaluate whether the material makes fair use of the copyright.” Lenz v. Universal, 572 F. Supp. 2d 1150, 1155 (2008)*

The Open Source Center reserves the right to review the allegedly infringing material and independently determine whether it is infringing.

Please also note that the information provided in this legal notice will be forwarded to the person who provided the allegedly infringing content. A copy of this legal notice may also be sent (with your personal information removed) to a third-party that may publish and/or annotate it for noncommercial research and educational purposes.

Counter-Notification: What You Can Do If Your Content Was Removed
-----------------------------------------------------------------

If you believe material you posted to a Open Source Center website was not infringing, you can submit a counter-notice. If you need assistance in determining whether the material was not infringing, please find an independent attorney to evaluate your situation.

A counter-notification must include the following:

1. Identification of the specific URLs of material that the Open Source Center has removed or to which the Open Source Center has disabled access.
2. Your full name, address, telephone number, and email address.
3. The statement: "I consent to the jurisdiction of the Federal District Court for the district in which my address is located, or if my address is outside of the United States, the judicial district in which the Open Source Center intellectual property owner is located, and will accept service of process from the claimant." The current intellectual property owner for the Open Source Center is United Nations Foundation, Inc., which is incorporated in the Southern District of New York.
4. The statement: "I swear, under penalty of perjury, that I have a good faith belief that the material was removed or disabled as a result of a mistake or misidentification of the material to be removed or disabled."

A scanned physical signature or a valid electronic signature is fine.

Please send your counter-notice to Michael Downey via email (osc@digitalimpactalliance.org).

Please note that under Section 512(f) of the Copyright Act, any person who knowingly materially misrepresents that material or activity was removed or disabled by mistake or misidentification may be subject to liability. After we receive your counter-notification, we will forward it to the party who submitted the original claim of copyright infringement. Please note that when we forward the counter-notification, it includes your personal information. If you are concerned about protecting your anonymity, please consult with an attorney about other options.

After we send out the counter-notification, the claimant must then notify us within 10 business days that the claimant has filed an action seeking a court order to restrain you from engaging in infringing activity relating to the material on the Open Source Center website(s). If we receive such notification we will be unable to restore the material. If we do not receive such notification, generally we will reinstate the material.

Please also be advised that in appropriate circumstances we will deny access to the Open Source Center websites by repeat infringers.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
