.. _osc-community:

###########
How We Work
###########

**********
Governance
**********

**The Center will be multi-stakeholder, participatory, and open.** Strategy will be guided by a **Governance Advisory Board (GAB)** whose members will include representation from participating partners (initially DIAL & PATH), participating projects, funders, sponsors, and other individuals as selected by the board over time. These staff will serve and convene the **Technical Steering Committee (TSC)** and **Community Steering Committee (CSC)**, both community-appointed groups implementing initiative-wide technical strategy and operational decisions.

.. figure:: gov.png
   :alt: Governance Structure

   Governance Structure

Open source projects may participate as more mature **member projects** or by enrolling as a **mentorship project**, in which OSC leaders such as the Director of Technology & Director of Community provide more hands-on guidance and operational support in establishing best practices within that project. Each mentorship project receives a customized set of services based on a gap analysis of needs. Day-to-day decision making for each project remains within each member project’s separate **Project Advisory Group**. Additionally, multiple projects may join together to form **Technical Interest Groups (TIG)** focused on cross-project technology questions or implementation challenges, e.g., the use of universal Digital ID’s across health and financial sectors. **Sustainability Advisory Groups** will be convened in multiple domains (health, finance, etc.) to connect projects to (and assist in proposing for) programmatic funding opportunities, and will be convened & operated in partnership with experts in that domain. Finally, **CSC Work Groups** will be established to handle challenges in cross-sector & cross-project collaboration challenges, e.g., documentation, diversity & inclusion, etc.

Governance Advisory Board
=========================

The OSC Governance Advisory Board (GAB) is a self-elected board serving as the Foundation’s management guiding branch of governance. The Board’s membership consists of individuals who are committed to steering the Open Source Center to advance its market and technical success, and who serve as positive ambassadors for the project. They manage non-technical aspects like marketing, legal, fundraising, and other management driving topics, together in consultation with the Managing Director. They also define the strategic direction for the Center and set policies and procedures to enable the Center to work toward those strategic goals. The GAB leaves all technical decisions to the open source meritocracy within member projects and the Technical Steering Committee (TSC). The Board is self-organized in respect to size and membership -- however, membership is constrained to ensure no more than one-third of voting members have a legal or financial relationship with any single third-party organization. At minimum, membership shall include the chairs of the TSC, CSC, as well as each sustainability advisory group. In order to provide outside perspectives, at least 10% of GAB membership shall be individual(s) having no other affiliation with the OSC.

Community Steering Committee
============================

The Community Steering Committee (CSC) is responsible for ensuring collaboration is the driving principle within the Open Source Center, its projects, as well as between member projects and the broader community. The CSC is also responsible for admitting new projects to the mentorship program as well as graduating projects from the mentorship program. The CSC Chair (Director of Community) sits on the Governance Advisory Board (GAB) and provides a conduit for communication between the management and community governing bodies within the OSC. Membership is comprised of the Director of Community as well as leaders from each participating project or team that does not have a primary focus of producing software, e.g. documentation, outreach, inclusion, etc.

Technical Steering Committee
============================

The Technical Steering Committee (TSC) is responsible for ensuring member projects are operating effectively to create software that delivers on each project's vision and meets the needs of constituents. The TSC will serve as the Center’s primary technical liaison body with external open source projects, consortiums and standards bodies. The TSC does not normally make everyday technical decisions on behalf of sub-projects, however it may assist those projects in decision-making when requested. The Committee may also facilitate coordinative decisions between member projects. It is also responsible for evaluating when and how mentored projects move through project lifecycle phases. The TSC consists of project leaders from each member project in an active state, the Director of Technology (who serves as a chief architect & consultant to sub-projects), and other members which the TSC may appoint from time to time. The TSC Chair also sits on the Governance Advisory Board (GAB) and provides a conduit for communication between the management and technical governing bodies within the Open Source Center.

Sustainability Advisory Groups
==============================

These groups exist as a service organization to client projects wishing to receive guidance & advice on sustainable funding opportunities for their projects. Sustainability Advisory Groups will exist in multiple domains/sectors, with Health being the first such established group. Members of each SAG will consist of experts in the domain, open source experts, and T4D experts generally.

OSC partner **PATH is planning to fund at least 1 person** to convene the **Health Sustainability Advisory Group** through its Digital Health Initiative (DHI). The chair of this group will assemble a group of peers to curate health-related digital development funding opportunities & assist with review of proposals by participating projects and other initiative participants.

Fiscal Sponsor
==============

Overall financial and legal management of the Open Source Center (OSC), as overseen by the Governance Advisory Board, is entrusted to one or more fiscal sponsors. A fiscal sponsor, as defined here, is a legally-organized nonprofit organization providing key services to the OSC community. They may occasionally provide legal representation, asset stewardship (such as trademarks or copyrights), and may hold money to be used as directed by the community’s leadership structures. The **Digital Impact Alliance (DIAL) at United Nations Foundation** is the current fiscal sponsor of the OSC.

*****
Roles
*****

The governing bodies of the Center will be staffed by both paid and unpaid individuals, who each participate independently, but at times may be financially or otherwise supported by an employer or 3rd-party organization to cover their time and efforts. Specifically:

Managing Director
=================

The managing director of the OSC will be responsible for public outreach of the organization’s initiatives, working to solicit OSC sponsors & member projects. The person serving in this role is appointed by the OSC’s fiscal sponsor with the advice & consent of the Governance Advisory Board (GAB). This role is also responsible for ongoing financial & resource management of the Center in collaboration with the fiscal sponsor. **Jeff Wishnie** currently serves in this role on an interim basis.

Director of Community
=====================

The Director of Community is a full-time role in service of member projects and the Community Steering Committee (CSC), and is appointed by the OSC’s fiscal sponsor with the advice & consent of the Governance Advisory Board (GAB). The individual in this role serves to manage community resources and communications, as well as to assist with promotion and marketing of the community and various member projects. The Director of Community has a permanent ex officio (non-voting) seat on the GAB and is charged to keep that group advised of current issues & concerns within the community. **Michael Downey** currently serves as the OSC Director of Community.

Director of Technology
======================

The Director of Technology is a full-time role in service of member projects and the Technical Steering Committee (TSC), and is appointed by the OSC’s fiscal sponsor with the advice & consent of the GAB. The individual in this role serves as a technical consultant and advisor to participating projects to assist with project architectural considerations. The role also serves to marshal infrastructure resources needed by various MSPs and provide them at a community-wide scale. Finally, serving as the chair of the TSC the individual in this role works to coordinate the work of various member projects and assists with project integration efforts. **David McCann** is the current OSC Director of Technology.

Future Roles
============

Additional **future roles may be served by volunteers** on an as-needed basis. DIAL will encourage partners and other participants to offset expenses of these volunteers.

************************
Long-Term Sustainability
************************

Over time, the Center itself may be sustained long term by a combination of **overhead fees** from both mentored & participating projects, related **business income** such as hosting of participating project software (e.g. SaaS offerings) for their clients, **consulting** fees generated by people serving in key project roles, **sponsorship** by supporting organizations, and **donations** from individuals.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
