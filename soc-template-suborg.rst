.. _soc-template-suborg:

===============================
Summer of Code Sub-Org Template
===============================

**Important:** Please make sure you've reviewed our :ref:`soc` documentation before starting your ideas page.

There are not very many strict requirements for Google Summer of Code Ideas pages, but there are some things that students often ask us for. This page is intended as a starting template for organizations so you don't forget those things.

**Note: In the past, many orgs have been rejected because their ideas pages were offline when Google checked. Make sure your ideas page is hosted somewhere that Google's Open Source Programs Office will be able to access when they check!**

**************
About MySubOrg
**************

Tell the students a bit about your organization. Here's some questions you might want to answer:

* What software are you creating?
* Why is it interesting?
* Who uses it?
* What languages is it written in?
* How is it going to change the world?

*******************
Contacting MySubOrg
*******************

* IRC/Chat system URL/directions/coordinates
* Mailing list and/or forum info

Be sure to list contact methods you actually use and will have mentors monitoring!

Include any special instructions/info about communicating, e.g.:

* What time zones are your mentors in? 
* Do you prefer it if GSoC students introduce themselves first (where?) or just dive in? 
* Are there any common mistakes students make when making a first impression?

*******************************
Getting Started as an Applicant
*******************************

Links to setup instructions, generally for "new developers", goes here. Some suggested things to answer:

* Where is the link to a setup guide for new developers?
* Are there any unusual libraries/applications that need to be installed first?
* What type of source control do you use? (include links to help and setup guides!)
* What's the process for submitting your first bug fix?
* Where should students look to find easy bugs to try out?

*****************************
Writing your GSoC application
*****************************

Links to (and advice about) applications and the application template goes here. **Remind your students that your sub-org name must be in the title of their applications!** As a sub org, all students will submit an application to the umbrella org.

* Here's a link to the master :ref:`soc-template-student` for DIAL Open Source Center.


******************
20xx Project Ideas
******************

*NOTE: You should usually have at least a couple of project ideas, ranging in difficulty from beginner to expert. Please do try to have at least one, preferably several beginner tasks: GSoC gets a lot of students with minimal open source experience who feel very discouraged (and sometimes even complain to Google) if orgs don't any have projects at their level.*

1. Project "Name"
=================

* **Project description:** Make sure you have a high-level description that any student can understand, as well as deeper details.
* **Skills:** programming languages? specific domain knowledge?
* **Difficulty level:** Easy/Intermediate/Hard classification. Students ask for this info frequently to help them narrow down their choices. Difficulty levels are something Google wants to see, so **these aren't optional;** make your best guess.
* **Related Readings/Links:** Was there a mailing list discussion about this topic? Other documentation? Standards you want the students to read first? Bug reports or feature requests?
* **Potential mentors:** A list of mentors likely to be involved with this project, so students know who to look for on chat/discussion venues if they have questions. If you've had trouble with students overwhelming specific mentors, feel free to re-iterate here if students should contact the mailing list to reach all mentors.

2. Project "Name"
=================

Same pattern as the previous project, etc. Unless there's a compelling reason to sort in some other order, **ideas should be ordered approximately from easiest to hardest.**