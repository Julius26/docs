.. _soc:

========================================
Summer of Code @ DIAL Open Source Center
========================================

.. image:: logo-gsoc.png
    :alt: Google Summer of Code

The `Digital Impact Alliance (DIAL) <http://digitalimpactalliance.org/>`_ at `United Nations Foundation <http://unfoundation.org/>`_ has launched its `Open Source Center <http://www.osc.dial.community/>`_ to provide a collaborative space for (and professional technical assistance to) open source projects focusing on international development and humanitarian response. The Center assists in the establishment of effective governance, software project management, and contribution models for member projects. It also provides technical, architectural, and programming support for projects; and assists those projects in support, engagement, management of their communities of contributors and implementers.

`Google Summer of Code (GSoC) <http://summerofcode.withgoogle.com/>`_ is a **global program that offers post-secondary students an opportunity to be paid for contributing to an open source project over a three month period.** The DIAL Open Source Center intends to serve as an "umbrella organization" to a variety of international development and humanitarian response related projects, to help raise awareness about those projects among student applicants, connect students to meaningful open source work that makes the world a better place, and help increase the maturity & reach of those projects.

The 2019 DIAL Open Source Center (OSC) GSoC coordinator is Michael Downey, the OSC's Director of Community. (downey on Freenode IRC, downey (at) dial (dot) community, but please email gsoc (at) dial (dot) community if you are a mentor who wishes to contact an admin. Students should almost always visit Getting Started first, and then email gsoc (at) dial (dot) community only if you get stuck.

**We are once again participating in GSoC as an umbrella org for the 2019 season!** If you are a potential student, please review this information now to review all of the DIAL associate organizations and their project ideas for 2019. You should then contact the sub-org directly to discuss project ideas before you prepare your application. If you are a current sub-org or one of our associate projects, and need to to update the information below, please get in contact with us. 

.. contents:: On This Page
   :backlinks: top
   :depth: 2
   :local:

**See also:**

* :ref:`soc-template-suborg`
* :ref:`soc-template-student`

.. _soc-ideas:


******************
DIAL Sub-Orgs & Associate Members
******************

Our sub-orgs & associate orgs have developed their GSoC ideas lists, which are available below. This is the perfect time to get to know them and work on proposal ideas together! Keep watching the sub-org ideas lists, which may be updated through the start of the GSoC application period. 

This section contains information about sub-orgs and their project ideas once they have gotten in touch with the OSC. If you're unsure whether your favourite project will be participating, ask them and encourage them to sign up! If you represent a T4D or HFOSS sub-org and want to be added to the list, read :ref:`soc-suborg`, then contact gsoc (at) dial (dot) community to be added to this page and get assistance with customizing the :ref:`soc-template-suborg` for your specific projects.

OSC participating projects may also be taking part in GSoC as "standalone" organizations, such as these last year in 2018, after having participated during previous years. These open source projects did not operate under the OSC umbrella, but they also serve international development and humanitarian response. Please visit their project ideas pages too!


Humanitarian Open Street Map Team (HOT)
=======================================

.. image:: 2018-hotosm.png
    :alt: Humanitarian Open Street Map Team (HOT)
    :width: 200

The Humanitarian OpenStreetMap Team (HOT) applies the principles of open source and open data sharing for humanitarian response and economic development.

* Project website: https://www.hotosm.org/
* Mailing list: https://lists.openstreetmap.org/listinfo/hot
* Chat: https://slack.hotosm.org/
* Technical coordination resources: https://github.com/hotosm/tech
* **GSoC ideas page:** https://github.com/hotosm/tech/blob/master/project-ideas/google-soc/soc2019.md


Ushahidi
========

.. image:: 2019-ushahidi.png
    :alt: Ushahidi
    :width: 200

Ushahidi platform is often used for crisis response, human rights reporting, and election monitoring. Ushahidi offers products that enable local observers to submit reports using their mobile phones or the internet, while simultaneously creating a temporal and geospatial archive of events. Ushahidi (Swahili for "testimony", closely related to shahidi which means "witness") created a website in the aftermath of Kenya's disputed 2007 presidential election that collected eyewitness reports of violence reported by email and text message and placed them on a Google Maps map.

The Ushahidi platform has been deployed over 125,000 times in over 160 countries, used by the Obama Campaign for America 2012, the United Nations Department of Field Services and Peacekeeping, in response to the Haiti Earthquake in 2010, to monitor the Nigerian elections in 2011, by the Nepalese army to respond to the earthquake of 2015, in and by local activists groups such as Humanitarian Tracker to monitor violence in the Syrian civil war and HarassMap to help women report on sexual harassment.

* Project website: http://ushahidi.com/
* Forum: http://forums.ushahidi.com/
* Mailing list: http://list.ushahidi.com/
* IRC chat: ircs://chat.freenode.net/#ushahidi
* Getting started as a developer: https://www.ushahidi.com/support/add-code-to-ushahidi
* **GSoC ideas page:** https://github.com/ushahidi/platform/issues/3484

**This organization is also participating in Outreachy's mid-2019 round!** Read more about Outreachy and how eligible people can appy at https://www.outreachy.org/ -- then read about this community's Outreachy projects at: https://www.outreachy.org/may-2019-august-2019-outreachy-internships/communities/ushahidi/


mUzima
======

.. image:: 2019-muzima.png
    :alt: mUzima
    :width: 200

mUzima is an open source, Android-based mHealth platform whose current version inter-operates seamlessly with OpenMRS. The Android app supports patient registration, data collection and upload, historical patient data access, cohorts management, patient search, form templates download, alerts and reminders, tele-consultation, and works both online and offline. At the server side, mUzima has modules to support forms management, form data processing, error resolution, and management of consultations.

* Project website: https://www.muzima.org/
* Wiki: https://wiki.muzima.org/display/muzima/muzima
* IRC chat: https://www.irccloud.com/#!/ircs://irc.freenode.net:6697/%23muzima-dev
* Developer Guide: https://wiki.muzima.org/display/muzima/Developer+Guide
* **GSoC ideas page:** https://wiki.muzima.org/display/muzima/Summer+of+Code+2019

**This organization is also participating in Outreachy's mid-2019 round!** Read more about Outreachy and how eligible people can appy at https://www.outreachy.org/ -- then read about this community's Outreachy projects at: https://www.outreachy.org/may-2019-august-2019-outreachy-internships/communities/muzima/

Mifos
=====

.. image:: 2018-mifos.jpg
    :alt: Mifos
    :width: 200

Mifos X is an extended platform for delivering the complete range of financial services needed for an effective financial inclusion solution. As the industry’s only open platform for financial inclusion, we provide affordable, adaptable and accessible solutions for any segment of the market, new and small financial institutions can easily start with our community app in a hosted environment, medium and large institutions that are evolving into full-service providers of financial inclusion can use our global network of IT partners to configure a Mifos X solution, and innovators can build and scale entirely new solutions on our API-driven platform.

* Project website: https://mifos.org/
* Mailing list: https://lists.sourceforge.net/lists/admin/mifos-developer
* Chat: https://gitter.im/openMF/mifos
* **GSoC ideas page:** https://mifosforge.jira.com/wiki/spaces/RES/pages/812810251/Google+Summer+of+Code+2019+Ideas

**This organization is also participating in Outreachy's mid-2019 round!** Read more about Outreachy and how eligible people can appy at https://www.outreachy.org/ -- then read about this community's Outreachy projects at: https://www.outreachy.org/may-2019-august-2019-outreachy-internships/communities/mifos/


LibreHealth
===========

.. image:: 2018-librehealth.png
    :alt: LibreHealth
    :width: 200

LibreHealth is collaborative community for free & open source software projects in Health IT.

* Project website: http://librehealth.io/
* Forum: https://forums.librehealth.io/tags/gsoc2019
* Chat: https://chat.librehealth.io/
* **GSoC ideas page:** https://forums.librehealth.io/t/librehealth-accepted-into-google-summer-of-code-2019/2932

**This organization is also participating in Outreachy's mid-2019 round!** Read more about Outreachy and how eligible people can appy at https://www.outreachy.org/ -- then read about this community's Outreachy projects at: https://www.outreachy.org/may-2019-august-2019-outreachy-internships/communities/librehealth/

Open Data Kit
=============

.. image:: 2018-odk.png
    :alt: Open Data Kit
    :width: 200

Open Data Kit (ODK) is a free and open-source set of tools which help organizations author, field, and manage mobile data collection solutions.

* Project website: https://opendatakit.org/
* Forum: https://forum.opendatakit.org/tags/gsoc-2019
* Chat: http://slack.opendatakit.org/
* **GSoC ideas page:** https://forum.opendatakit.org/t/welcome-gsoc-2019-applicants/18160

Public Lab
==========

.. image:: 2018-publiclab.png
    :alt: Public Lab
    :width: 200

Public Lab is a community and non-profit democratizing science to address environmental issues that affect people. In Public Lab, we believe that generating knowledge is a powerful thing. We aim to open research from the exclusive hands of scientific experts. By doing so, communities facing environmental justice issues are able to own the science and advocate for the changes they want to see.

The science, technology and data in Public Lab are community-created and open source. We utilize our open data to advocate for better environmental management, regulations and enforcement. These tools enable people to more easily generate knowledge and share data about community environmental health.

* Project website: https://publiclab.org/
* Mailing list: https://groups.google.com/group/plots-dev
* Chat: https://publiclab.org/chat
* **GSoC ideas page:** https://publiclab.org/wiki/gsoc-ideas

**This organization is also participating in Outreachy's mid-2019 round!** Read more about Outreachy and how eligible people can appy at https://www.outreachy.org/ -- then read about this community's Outreachy projects at: https://www.outreachy.org/may-2019-august-2019-outreachy-internships/communities/public-lab/

**************************
Former Years DIAL Sub-Orgs
**************************

These OSC participating projects took part in GSoC during past years as sub-organizations, and serve international development and humanitarian response. Although they are not participating in Google Summer of Code this year, they are always looking for contributions!

OpenLMIS
========

.. image:: 2018-openlmis.png
    :alt: OpenLMIS
    :width: 200

OpenLMIS is an open source, cloud-based, electronic logistics management information system (LMIS) purpose-built to manage health commodity supply chains. The OpenLMIS initiative incorporates a community-focused approach to develop open source and customizable LMIS systems specifically designed for low-resource settings.

The latest version of OpenLMIS, the version 3 series, uses a microservices architecture deployed with Docker. OpenLMIS is currently deployed in Benin, Côte d’Ivoire, Malawi, Mozambique, Tanzania, Zambia, and Zanzibar where it manages logistics processes in health commodity supply chains for over 10,000 health facilities.

* Project website: http://openlmis.org/
* Wiki: https://openlmis.atlassian.net/wiki/spaces/OP/overview
* Mailing list: https://groups.google.com/forum/#!forum/openlmis-dev
* Slack chat: `Request an invite <https://join.slack.com/t/openlmis/shared_invite/enQtMzAyOTU2NDg0NDY5LWYxMTM0ODczZWE1OWY3YzA3NGZhOGNmOGZjMWQzYjQwMmE5ZGYwZDdiZjQ5NDY5MDRlODQ2Y2Y2Y2E1OGFiYWQ>`_
* **GSoC ideas page:** `Visit the OpenLMIS Wiki <https://openlmis.atlassian.net/wiki/x/XoBCD>`_

SUMSarizer
==========

.. image:: 2018-sumsarizer.png
    :alt: SUMSarizer
    :width: 200

SUMSarizer helps researchers measure impacts of improved cookstoves by using machine learning to turn raw data from stove use monitoring sensors (SUMS) into summaries of cooking events.

* Project website: http://sumsarizer.com/
* Forum discussion: https://forum.osc.dial.community/c/community/soc
* Developers chat: https://dial.zulipchat.com/#narrow/stream/OSC.20Summer.20of.20Code
* **GSoC ideas page:** `Consult the SUMSarizer Wiki <https://github.com/SUMSarizer/SUMSarizer/wiki/Google-Summer-of-Code-2018>`_

.. _soc-students:

************************
Information for Students
************************

GSoC is basically an open source apprenticeship: students will be paid by Google to work under the guidance of mentors from an open source community. It's a really great opportunity to build new skills, make connections in your community, get experience working with a larger and often distributed team, learn, and, of course, get paid. If you're new to GSoC and what it means to be a student, learn more in this short video:

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/S6IP_6HG2QE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

Students are expected to work around 40 hours a week on their GSoC project. This is essentially a full-time job. Ideally, you should not attempt to do another internship, job, or full-time schooling while you're doing GSoC.

Remember that Google intends this to be a way for new contributors to join the world of open source. The students most likely to be selected are those who are engaged with the community and hoping to continue their involvement for more than just a few months.

To apply, you need to take a look at the mentoring organizations and the ideas that they are willing to sponsor. See the "Ideas" section of this page for our sub-orgs and their project ideas. Typically, you'll choose one of their ideas and work with a mentor to create a project proposal that's good for both you and your chosen open source community. Sometimes, projects are open to new ideas from students, but if you propose something new make especially sure that you work with a mentor to make sure it's a good fit for your community. Unsolicited, un-discussed ideas are less likely to get accepted.

**Note that the DIAL Open Source Center (OSC) is an "umbrella organization" which means that our team is actually a group of projects that work together to do Google Summer of Code.** If you're going to apply with us, you'll need to choose from one of those teams, because that defines which mentors will be helping you with your applications. **Applications without any sub-org and mentor to evaluate them will be rejected.** You can work with more than one sub-org while you're figuring out what you want to do, but you can only accept one "job offer". Here's some resources so you can read up more on how to be an awesome student:

- `The GSoC student Guide <https://google.github.io/gsocguides/student/>`_ -- This is a guide written by mentors and former students. It covers many questions that most students ask us. Please read it before asking any questions on the mailing list or IRC if you can! New students in particular might want to read the section `Am I Good Enough? <https://google.github.io/gsocguides/student/am-i-good-enough>`_
- `Google's list of resources <https://developers.google.com/open-source/gsoc/resources/>`_ -- Note especially the `Frequently Asked Questions (FAQ) <https://developers.google.com/open-source/gsoc/faq>`_ which does in fact answer 99% of the questions students ask.
- Our OSC Summer of Code :ref:`soc-faq` will answer the questions that we most often get about OSC projects. You might want to see "How do I choose a project or a sub-org?" or "How many slots does OSC get?"

.. _soc-students-apply:

Students: How to Apply
======================

**Communication is probably the most important part of the application process.** Talk to the mentors and other developers, listen when they give you advice, and demonstrate that you've understood by incorporating their feedback into what you're proposing. We reject a lot of students who haven't listened to mentor feedback.

If you're interested in how to prepare a strong proposal, take a look at these tips from past mentors:

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YN7uGCg5vLg?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

Short application checklist:

#. Read the links and instructions given on this page -- All of it! we've tried to give you all the information you need to be an awesome student applicant.
#. Choose a sub-org from our list. Applications not associated with a sub-org typically get rejected.
#. Talk to your prospective mentors about what they expect of student applicants and get help from them to refine your project ideas. Listening to your mentors' recommendations is very important at this stage!
#. Prepare a patch or merge/pull request for that sub-org.
#. Set up a blog where you will keep track of your GSoC progress.
#. Write your application (with help from your mentors!) Make sure to include the sub-org name in the title so your mentors can find it in the system, and make it easy for your mentors to comment on your doc. e.g., Enable comments if you're using Google Docs. See :ref:`soc-template-student` for the application template with notes and tips.
#. Submit your application to Google **before** the deadline. We strongly recommend you submit a few days early in case you have internet problems or the system is down. Google does not extend this deadline, so it's best to be prepared early! You can edit your application up until the system closes.

.. _soc-dates:

***********************
Important Program Dates
***********************

**Next Deadline: March 25 - April 9, Students register & submit applications to organizations no later than April 9 at 11:00 PDT.**

Current Key Dates:
------------------

* **February 26:** Google announces list of accepted mentoring organizations.
* **March 25 - April 9:** Students register & submit applications to organizations no later than April 9 at 11:00 PDT.
* **May 6:** Student pojects announced; students & mentors start planning their projects and milestones.
* **May 27 - August 19:**  Students work on their Google Summer of Code projects.

Other dates and deadlines for 2019 will be added once we get accepted, so check back in February.

Please note `Google's official GSoC dates and deadlines <https://summerofcode.withgoogle.com/how-it-works/#timeline>`_. It is the final word when it comes to any deadlines.

In general, OSC will ask mentors to do things 48h before the Google deadline. This allows our admins time to make sure that evaluations, etc. are complete and ready for Google when their deadline comes. Student deadlines remain the same, although getting things done earlier is never a bad idea!

.. _soc-faq:

********************************
Frequently Asked Quetsions (FAQ)
********************************

Getting Started
===============

How do I get started in open source?
------------------------------------

Here are the top things you can do to get started in free and open source software:

#. Choose an organization to work with.
    - There are thousands of open source software projects, and the list of projects focused on international development is also long. You need to narrow down the list before you can get help or do much that's useful. See "How do I choose a project or sub-org?" for ideas on how to do that.
    - Any open source experience will help you prepare for GSoC, so don't worry too much about what project you try first and don't be afraid to change your mind!
    - For GSoC applications, you'll need to choose from the list of accepted OSC sub-orgs, or Google's master list of participating orgs. If your favorite group isn't on the list, contact them to see if they're interested in participating. Student applications not associated with a known sub-org are usually rejected because we don't have mentors available.

#. **Set up your own development environment.** This will vary depending on what project(s) you are interested in, but they should have documentation and help for you along the way.
    - Be sure to document what you do so you can remember it later, and so you can help others if they get stuck! And if you get stuck, don't be afraid to ask for help.

#. **Start communicating with the developers.** Join the mailing list, IRC channel, or any other communication channels the developers use. Listen, get to know the people involved, and ask questions.
    - In almost all cases, you should **communicate in public** rather than in private. GSoC is a busy time for many developers and many beginner questions get asked repeatedly. Help keep your devs less stressed out by reading what they've written already and making it easier for them to have a record of the things they've answered. You can use a pseudonym/nickname if you want. Also, search those archives to make sure you're not asking something that's just been asked by someone else!
    - If you want to make the best first impressions, DO NOT start with "Dear Sir." and DO NOT ask to ask. See our :ref:`soc-faq` for details.

#. **Find some beginner-friendly bugs and try to fix them.** Many projects have these tagged as "easy" "bite-size" or "beginner-friendly" so try searching for those terms or looking at the tags to figure out which bugs might be good for you. Sub-org project ideas page will point you to them.
    - Note that if you apply as a student with the OSC you will be asked to submit a code sample -- generally code related to your project. A few fixed bugs with code accepted upstream will make your application look great!
    - Remember, competition for easy bugs is very high during GSoC so it can be hard to find one that's tagged. If you don't see anything from your search, read through the bugs and choose a few that sound like something you can fix. Remember to ask for help if you get stuck for too long, "I'm a new contributor and was trying to work on bug #123456. I have a question about how to..." -- if people can't help, sometimes they will be able to suggest another bug which would be more beginner-suitable.
    - Other "easy" bug ideas: find typos and fix them. Set up new tests -- even if the project doesn't need the first one you write, practice writing test cases is useful for later. Talk to your org's community to find out more about what they're doing with testing.

#. **Find bugs and report them.**
    - Hopefully you won't encounter too many, but it's always a good idea to get familiar with your project's bug reporting process.

#. **Help with documentation.** As a beginner in your project, you're going to see things that are confusing that more experienced developers may not notice.
    - Take advantage of your beginner mindset and make sure to document anything you think is missing!

#. **Help others.** This is a great idea for a lot of reasons:
    - Explaining things can help you learn them better.
    - Demonstrating your skills as a good community member can make you more memorable when your mentors have to choose candidates.
    - Being helpful makes your community a better place!

How do I choose a project or sub-org?
-------------------------------------

Choosing a project is a pretty personal choice. You should choose something you want to work on, and none of us can tell you exactly what that would be! But here's a few questions you might want to ask yourself to help figure that out:

* **How do you want to change the world?** Do you want to help people learn more? Communicate better? Understand our world better? With the DIAL Open Source Center, all of our projects are designed to help improve communities and peoples' lives, so you'll have choices!
* **What would you like to learn?** GSoC is meant to be a bit of a learning opportunity. Have you always wanted to be more involved with health? Data? Visualization? Education? See which projects might help you improve your skills.
* **Who do you like working with?** Hang out where the developers do and get to know some of your potential mentors. Which developers inspire you?
* **How do you like to communicate?** Do you like realtime chat communication? Perhaps you should choose a project with mentors in a time zone close to you. Do you like asynchronous communication on mailing lists or forums? Find a group with active discussions. Communication is a big part of Summer of Code (and really any open source development in a team!) so finding a team that works the way you want to work can make your experience more awesome.

A list of sub-orgs for this year will be published on this site.

**If you're chosen as a GSoC student, you're going to be expected to make some decisions on your own, so you can make a better first impression on mentors by showing that you're able to narrow down your field of choices!**

What do I need to know to participate in Summer of Code with OSC?
-----------------------------------------------------------------

The answer to this depends a lot on the project you choose. We have a range of projects, from beginner to advanced. Every sub orgs expects different things from their students. Maybe you'll need to know a bit about machine learning, or email, or image processing. The best answer to this question is, "always ask your mentors what you will need to know" for a specific project.

But a lot of people ask early on because they want to be sort of generically ready, but they're not sure what they want to do yet. So the above answer is not always super helpful.

In that case, here's a list of a few things that are useful for most OSC projects:

* **You must have some experience with software development.** You can be a beginner, but practicing in advance is good! And there are a lot more projects available for students who are reasonably used to a project's specific language, so more practice means you'll have more project options.
* **You need to feel comfortable asking questions,** because we're going to expect you to ask if you don't understand something.
* **You should be comfortable communicating your ideas to others in public.** Most projects have public mailing lists or forums, and would prefer if you use them. OSC students will also be asked to blog about their work over the summer. You can use a pseudonym (nickname) if that works best for you. Google will need to know who you are to pay you, but we just need something to call you.
* **You probably want some experience with version control.** We have a lot of projects that use different tools, such as Git, Mercurial, or Subversion, and you can find out which one your project uses in advance and practice using it on your schoolwork or personal projects to get used to it.

Communication
=============

What does "don't ask to ask" mean?
----------------------------------

You'll hear this phrase sometimes on IRC or other chat systems, and it means, "Please just ask your question, don't say something like 'can I ask a question?' first."

Why? Developers are often pretty busy, and if you just ask the question, someone can jump in the minute they see your message with the answer or direct you to folk who can answer it better.

If you ask "can I ask a question?" you're basically just waiting for someone to say "yes" before any useful information is communicated. Many folk consider this slow, annoying, and perhaps even rude. Save everyone (including yourself!) some time and just ask the question right from the start. Culturally speaking, in open source projects it's generally ok launch right in to a question on IRC; you don't even have to say hi first!

What should I do if no one answers my question?
-----------------------------------------------

* Be patient. If you're on IRC or another chat tool, stick around for an hour or so (you can do something else, just leave the window open and check back occasionally) and see if someone gets back to you. If they don't, try posting to the forum or mailing list. (It's possible all the developers are asleep!) You should give people around 24-48h to answer before worrying too much about it.
* Make sure you're asking in the best place. One common mistake students make is to contact individual developers rather than asking on a public mailing list or a public IRC/chat channel. You want as many people as possible to see your questions, so try not to be shy! (Don't worry about looking too much like a newbie -- all of us were new once!) Sometimes projects have different lists/IRC channels/forums/bug queues for different types of questions. If you're not sure, do feel free to follow up your question with something like, "Hey, I haven't gotten an answer on this. Is there somewhere better I could post it or more information you need to help?"
* Try giving more information. If you've hit a bug, try to give the error message and information about your setup and information about what you've already tried. If you're trying to find a bit of documentation, indicate where you've already looked. And again, "Hey, I haven't got an answer; what other information could I provide to help debug this problem?" is usually a reasonable follow-up if you're not sure what people might need.
* If you're really having trouble getting in touch with your mentors, talk to the OSC GSoC team emailing gsoc (at) dial (dot) community. The GSoC org admins should have contact info for mentors with each project and can help connect you. (Note: Please don't complain that you can't get in touch with us on the general Google lists or the global #gsoc IRC channel. They're just going to redirect you to the OSC org admins anyhow!)

How should I address my emails?
-------------------------------

*(Or, "Why shouldn't I start my emails with 'Dear Sir'?")*

If you want to make the best first impression, **do not start emails with "Dear Sir."** OSC has many mentors who are female and/or prefer other forms of address. We realize you're trying to be polite, but "Dear Sir" is often perceived in our communities as alienating, rude or simply too formal and off-putting.

Try "Dear developers" or "Dear mentors" if you're sending a general email. If you're addressing a specific person, use the name or nickname that they use on their emails. Culturally speaking, first names or chosen nicknames are fine for most open source projects.

Mentoring
=========

What does it take to be a mentor?
---------------------------------

* We expect around a 0-10hr/week commitment, which sounds scary, but it's not actually that variable. You usually spend up to lots of time for the first few weeks, where you're fleshing out your ideas page, discussing projects with many students, and selecting students from their proposals. After students are selected, it becomes more like a 1hr commitment per week for a weekly meeting, and maybe a few more hours here and there for code review or questions. (That depends on your student: experienced students may need very little supervision, inexperienced students may need more. It also depends on you: You and your co-mentor(s) select the student and project you mentor, so you can choose according to the time commitment you have. Some mentors even do pair programming with their students!)
* We want at least two mentors per project, so hopefully no one ever gets overwhelmed and feels like they're always on call. Google does ask that we try to answer questions within 48h so students can't get stuck for too long. Remember, no one mentor has to know all the answers.
* We recommend at least one mentor has a weekly 1hr meeting with the student so they get to know each other, keep everyone on track, and give a chance to talk about other stuff. Lots of students have questions about jobs, courses, architecture, open source, etc. and it's nice to have someone to talk to). Some weeks this meeting may be the only mentoring time needed.
* Mentors don't have to be the "Best At Everything". A lot of what mentors do is keep students on track and keep them from getting stuck for too long. Sometimes that means just knowing who to ask or where to look rather than knowing the answer yourself. In an ideal world, at least one mentor can answer at least basic architectural questions and knows how to get code accepted upstream, though.
* Mentors do have to do multiple evaluations on the student, two mid-terms and one at the end. (only one evaluation per student per period, though, so only one mentor needs to do this). There's a few questions about how the student is doing and then a pass/fail that determines if the student gets paid and continues in the program.

Other
=====

How many slots does OSC get? How many slots does project X get?
---------------------------------------------------------------

We don't know our slot allocation until Google announces them, and Google bases their numbers on the number of students we tell them we want. The more great applications we have, the more slots we'll request. So rather than worrying about the number of slots, you should be aiming to be such a memorable and great prospective student that your sub-org will definitely request a slot with you in mind.

For sub-orgs, new groups working with us usually get 1-2 slots, experienced sub-orgs may be granted as many as they can comfortably mentor at the discretion of the org admins. The maximum number will likely be close to the total number of mentors divided by two, but the actual number requested depends on which students the org specifically wants to accept after they've done an initial review of the applications.

We anticipate being more limited by the matching of mentors with truly excellent students.

.. _soc-suborg:

***********************************
Participating as a Sub-Organization
***********************************

First of all, make sure you understand what's involved and the benefits of participating in Google Summer of Code. Watch this short video first, and drop us a line at gsoc (at) dial (dot) community if you have questions!

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/L4JNz6zWzLs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

To participate under the DIAL Open Source Center (OSC) umbrella, a sub-organization must do the following:

#. Be an open source project that is focused on international development, humanitarian response, or other such project that is aligned to focus on `the DIAL Open Source Center challenge <http://www.osc.dial.community/challenge.html>`_.
#. Meets `Google's requirements <https://summerofcode.withgoogle.com/rules/>`_ for GSoC.
#. Have one sub-org admin and at least two mentors who are willing to commit to the full GSoC period. (The admin can also be a mentor, but the more people, the better!)
#. All sub-org admins and mentors agree to follow the :ref:`OSC Code of Conduct <code-of-conduct>` for the duration of the program.
#. Send an email indicating interest to gsoc (at) dial (dot) community, before the announced deadline. Exceptions can be made if you get an amazing student applicant later and want to sign up just for them.
#. Have a good ideas page. See :ref:`soc-template-suborg` for an example. Getting a really great page sometimes takes a few rounds of revisions. We will work with you to make sure your page is ready! Once you're ready for review, you can send request to be added to this page.
#. Be able to handle meeting deadlines and following both Google and OSC's rules. We try to send important reminders for big deadlines, but we only have limited volunteer time for nagging and cajoling. Groups that cause repeated problems may be asked to take time off to limit volunteer burnout.

We can't promise to take everyone who meets those criteria, but we do try to take any eligible project/organization that we feel will give the students a great experience. The OSC Director of Community has final say in which projects participate under the OSC umbrella, but please send any queries to the team at gsoc (at) dial (dot) community to make sure we're all on the same page.

T4D, HFOSS, and other international development focused projects are welcome and encouraged to apply as separate mentoring organizations directly with Google. We're happy to help you in any way we can and we don't mind being your backup plan. We're also happy to help advertise related organizations not under our umbrella -- we want students to find projects that best suit them!

.. _soc-cfm:

Mentors Wanted!
===============

Interested in volunteering with the DIAL Open Source Center or one of our participating projects?

**The biggest job is mentoring students:** Mentoring a student as a primary mentor can be a pretty big time commitment. We recommend around 0-10 hours a week on average for the 3 months of the program, with more time at the beginning and less once the student learns to work independently. It's a very rewarding chance to give a student an open source apprenticeship. Mentorship happens in teams, so even if all you can handle is a few code reviews or taking over for a week while someone's on vacation, our projects still love your help.

**The easiest way to become a mentor is to be part of one of the sub-orgs that plan to be involved, so get in touch with them directly if you want to help.** If you're part of a group that would like to participate as a sub-org, please read the section for sub-orgs below.

Google produced a great video with advice on being a great mentor. Take a look:

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3J_eBuYxcyg?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

But our projects also need other volunteers! We're also looking for friendly community members to help with other tasks! We'd love to have more people available on IRC, mailing lists, and forums to answer student and mentor questions in various time zones. We are particularly looking for volunteers who can read and comment on student blogs, remind students if they haven't posted, and promote the work our students do to the larger open source & international development community. Or maybe you have another skill set you'd like to contribute? (Proofreading? Recruiting diverse student applicants?) If you want to help, we'd be happy to find a way to make that happen!

If you'd like to volunteer, get in touch with a sub-org admin or email our GSoC team at admins at gsoc (at) dial (dot) community.

.. _soc-contact:

****************
Getting in Touch
****************

* Please note that :ref:`the OSC has a Community Code of Conduct <code-of-conduct>` and mentors and students working with the OSC are asked to abide by it as members of the community.
* Sign up to our forum at https://forum.osc.dial.community/ list to get updates, reminders, and to discuss questions.
* https://dial.zulipchat.com is our real-time chat service. Zulip is an open source project similar to IRC or Slack. Please visit the "#OSC Summer of Code" channel with questions or ideas!
* Found a typo? Want to improve this page? Use the "Edit on GitLab" link above and submit a merge request!
* To get in touch with people from a specific sub-org, check their ideas page listing.

Please try to read all the information on this page before asking a question. We have tried to answer a lot of common questions in advance!

**Remember to be patient:** Our mentors generally have day-jobs and are not always paying attention to chat, mailing lists, or forums. (This is especially true during GSoC off-season. Expect more active mentors after Google's announcement of organizations.) Please ask questions directly on channel (you don't need to introduce yourself or say hi first) and please be patient while waiting for an answer. You could wind up waiting an hour or much longer for "real-time" answers if all the mentors are in meetings at work or otherwise occupied. If you can't stay that long, stay as long as you can and then send email to the mailing list instead so mentors have some way to reach you. We try to answer emails within 48h.

**For mentors:** The OSC GSoC team can be reached at gsoc (at) dial (dot) community if you or students have questions about participating.

.. _soc-ack:

****************
Acknowledgements
****************

This work was adapted from "Google Summer of Code 2018 @ the Python Software Foundation" by Python Software Foundation, used under the Creative Commons 4.0 International Attribution License (CC BY 4.0). Our umbrella org strategy has also been adapted from the time-tested policies, processes, and content from this excellent team. They have also provided mentorship and advice to our umbrella program. Thank you!
