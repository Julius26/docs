.. _onboarding:

####################################
Steps for Getting Started in the OSC
####################################

Dear prospective OSC participant,

Thank you for your interest in the DIAL Open Source Center. We are eager to move forward to help your project reach maximum success!

Our process for becoming an OSC member project & receiving services is outlined in our :ref:`current services overview page<services>`. In short, your project will want to (a) sign on as a member, (b) discuss & plan the most useful services available on a quarterly basis, and (c) propose & negotiate available services together with the OSC governance board.

******************
Membership Process
******************

The first step would be for your project to officially sign on as a "member project". Similar to endorsement of DIAL's Principles for Digital Development, this means that your project's leadership & governance, at the highest levels, aligns with the OSC's mission & vision, and seeks to achieve that vision through the project's policies, processes, and activities.

Membership is a three-step process that requires: (1) written authorization to include the project's name and/or logo as an participating member, (2) project participation in OSC member groups mutually deemed appropriate (e.g., discussion forums, sustainability advisory groups, or multi-project collaborative groups), and (3) an annual self-assessment, the results of which participating members self-publish on their own website or blog (and optionally in DIAL/OSC communication channels).

Participating members are expected to share senior-level signature on the following statement:

"As a participating member project in the DIAL Open Source Center, we will seek to further the OSC community's mission, values, and vision through our project's ethos, work culture, policies, and processes. We support the OSC's Four Goals for Communities, and together with other members, will to the best of our collective ability work to support other member projects' pursuit of those goals."

Details on the above mission, values, vision, and goals are available on our :ref:`"Why Join the OSC?" page.<membership-details>`

*****************
Services Proposal
*****************

After your project's leadership has agreed to be a participating member, you will work together with your OSC Governance Advisory Board (GAB) liaison to review our current list :ref:`services overview<services>` and :ref:`service details<services-details>` pages. Services are currently requested on a quarterly basis, beginning with the our first fiscal quarter, starting October 2017. Your liaison will help to set realistic goals for the coming quarter, and help you to prepare a written request to the GAB. The liaison will then (with your assistance) prepare a current :ref:`Project Maturity Evaluation<intake-evaluation>` and forward that evaluation, along with your written request and the liaison's recommendations, to the whole GAB for review in its next meeting.

The GAB will review your services proposal in consideration with the available resources for the OSC at large in the upcoming quarter, and will then issue a "green light" acceptance of your service proposal as-is, a "yellow light" request for changes, or a "red light" if there will not be sufficient services or there is otherwise a problem with your request. Your project would then be given an opportunity to revise, accept, or reject the offer, and services would begin as of the first date of the next quarter.

****************
Catalytic Grants
****************

We would also encourage your project to consider applying for the first round of our Catalytic Grantmaking program, to be offered during September 2017. :ref:`Additional details about the program are available online.<grants>` The OSC hopes to offer this program to its participating member projects on a rolling bi-annual basis, with each round having a unique theme of activity/work to be funded.

***************************************
Outreachy Diversity & Inclusion Program
***************************************

We encourage your project to participate in Outreachy, a mentorship program to increase open source participation by people in historically under-represented groups. The OSC is an "Equalizer" sponsor of Outreachy, and is allocated a number of internship "slots" within the program, divided between the Winter & Summer sessions. The OSC Community Steering Committee will work to grant these internship positions to participating members in advance of each program window. For more information, see http://outreachy.org/ or contact your GAB liaison.

*********************
Google Summer of Code
*********************

We also encourage your project to consider participating in Google Summer of Code. The OSC plans to participate as an "umbrella organization", allowing participating member projects to offer mentorship projects under the centralized management & operation by the OSC Community Steering Committee. While any project is always welcome to apply as a standalone GSoC organization, we believe this arrangement may reduce management overhead and increase your access to additional student projects during the summer. For more information, see http://summerofcode.withgoogle.com/ or contact your GAB liaison.

*******
Summary
*******

We’re looking forward to helping you move through this process and to begin connecting your project with resources as soon as possible. We would be happy to discuss any questions or concerns you have directly, for presentation in an upcoming meeting of your project's governing body, either by email or live text/voice conversation.

Cheers,
DIAL Open Source Center

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
