.. _grants-apply:

Greetings! 

Thanks for your interest in the DIAL Open Source Center and our first round of 
"catalytic grants" as we work to support free & open source software projects to 
serve people in the international development & humanitarian response sectors.

### DO NOT ADVANCE

The response to this round of funding was overwhelming, and we are unfortunately 
not able to move forward with all projects. We encourage you to keep an eye on 
our web site for our next round of catalytic funding, currently scheduled for 
around April 2018.

### ALL MESSAGES

As a reminder, here are the FOSS projects we are able to support in this first 
round:

1. In most cases, the project must already exist, and already have multiple 
stakeholders, e.g., people participating from multiple unique organizations. 
Single-person or single-organization projects will be unlikely to receive 
funding during this round.

2. Projects that were previously led by multiple stakeholders, but are currently 
"dormant" or reduced in scope, along with a plan for "re-invigorating" the 
project.

3. The project must already be available under an FSF or OSI approved license. 
Proprietary software projects will not be funded.

4. The project must have an existing reputation as, or clear potential case to 
be considered as, service to the "public good" in the humanitarian/development 
space. See https://en.wikipedia.org/wiki/Public_good for details.

5. The project (or at minimum the relevant aspect(s) of the project) should be 
well-aligned with the Principles for Digital Development and the mission 
statement of the OSC. http://www.osc.dial.community/mission.html

Regardless of funding in this round of grants, we encourage your project to 
consider applying to be a partcipating member in the Center, both to access 
future services & opportunities, as well as to join our community of FOSS 
projects working in the ICT4D space. We anticipate many future opportunities 
for collaboration as our brand-new program grows. See 
http://www.osc.dial.community/join.html for details and steps to apply.

### ADVANCE MESSAGES ONLY

Note: Grants are only available to OSC member projects, so your project will 
be required to apply if accepted.

We would like to extend to you an invitation to submit a proposal for funding in 
this round.

If you believe your project fits all 5 of the above criteria, you must prepare 
a short (1 page is ideal, 2 pages if necessary) three-section proposal that 
includes: (a) a narrative description of nature & rationale of work to be done 
- as well as success criteria, (b) a budget and plan for use of funds, (c) what 
party should receive the funds on your project's behalf. Review 
http://www.osc.dial.community/grants.html for examples, but generally you should 
know that we anticipate proposals falling into one of these three categories:

- Strengthening the project’s foundational base,
- Improving the project’s software quality & reliability, and/or
- Tending to often-neglected but high-value areas of the project.

Please make sure that you explain how your proposal's work will lead to a more 
"healthy, sustainable open source community and/or product". Your proposal 
should be sent by via email to grants@dial.community, and should use the subject 
line "Catalytic Grant Round 1: Proposal for {your project name}". It must be 
sent no later than 26 November 2017. Please allow yourself plenty of time and 
do not wait until the last minute. Late proposals can not be accepted.

If you have questions about your proposal, please post in our forums at 
https://forum.osc.dial.community/cg01 (preferably) or email us at 
grants@dial.community.

Once again, thank you for your interest. We look forward to reading your 
proposal!

### ALL MESSAGES

DIAL Open Source Center
Governance Advisory Board