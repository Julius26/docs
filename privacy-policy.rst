.. _privacy-policy:

Privacy Policy
==============

The Open Source Center is a free & open source service project organized by a worldwide group of volunteers. Our challenges and goals are large. For that reason, we have a very permissive (non) privacy policy that mirrors our public mission.

Please assume that everything you contribute intentionally to the Open Source Center is public. This includes messages on public forums, wikis, online discussions of all sorts, and software or documentation that you post to our websites.

Because we value mutual personal respect, disclosure of interests, and openness of expression, even your Open Source Center profile is public information. There will be cases where you are invited to share private information for various purposes, and denote that information as such. In those cases, we commit to only disclosing that information without your permission upon receipt of valid legal orders, and whenever possible, following notice from the Open Source Center to you.

Although what you disclose here is public, we are all limited by copyright and patent law in how we may use your contributions. Therefore, the Open Source Center only accepts voluntary contributions of software and documentation that are expressly licensed to the Open Source Center under an approved open source license. Refer to the Open Source Center Contribution Policy for additional details.

The Open Source Center welcomes your questions or comments regarding this Privacy Policy. Send them to osc@digitalimpactalliance.org.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
