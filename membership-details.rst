.. _membership-details:

#################
Why Join the OSC?
#################

The DIAL Open Source Center (OSC) is a collective group of individuals who believe in the common challenge, vision, values and goals, and who have expressed an interested in offering their time and talents to achieve those goals. They may be individuals who have independently discovered our group, or they may be associated with organizations doing work related to our mission. Regardless, the community welcomes each and every one of them and their potential contributions.

Join us! Start the conversation with an email to osc@digitalimpactalliance.org.

*************
The Challenge
*************

Over the past decade, the international development community has been exploring how the use of digital technologies -- including tools like the mobile phone, the Internet, and open source software -- can extend the reach of development. Simultaneously these same development organizations have struggled to leverage open source software in an effort to make their own work more participatory, sustainable, and effective. Mainstream software often used in wealthier markets does not always fully meet the specialized needs of international development projects and the areas in which those projects are undertaken. Other fields have demonstrated the open source software development model as a proven viable model to leverage global collaboration to share costs across institutions, increase the quality of products, and more rapidly innovate. In the development community, the results have been mixed. Relatively few of these open source projects have endured & matured -- when successful, enabling improved and sustained access to information and services that previously were out of reach for marginalized populations. Many more efforts have failed, often due to preventable reasons, resulting in countless development investments unable to scale. These open source digital development projects usually struggle with lack of long-term investments in key focal areas such as community effectiveness & product development. Without emphasis of these key considerations, they can rarely match the functionality and quality of their competitors. As a result, these open source projects can’t grow to the more advanced levels of maturity needed for widespread adoption throughout the development field.

Our mission
===========
The mission of the DIAL Open Source Center is to convene a vibrant, inclusive, free & open source software community that promotes knowledge sharing, collaboration, and co-investment in technology & human capacity to support positive social change in communities around the world. Together, our community will learn and demonstrate how to overcome key barriers in conceptualizing, designing, and building these software products, collectively building a public commons that can be scaled up to achieve global value.

Our values
==========

* We believe **technology has a key role to play in both ensuring that international development effectively uses resources,** as well as ensuring that those development programs empower beneficiaries to be active participants in society.
* We believe the **techniques evolved by the free & open source software movement are the best ways** to create inclusive, collaboratively-designed software – ensuring all stakeholders have a voice.
* We believe that **bringing related software development teams into a common virtual space is the best way** to learn from each other, innovate faster, and reduce duplication of efforts.
* We also believe that a **software development community with users actively engaged at its core is the best way** to ensure those users are the beneficiaries of tools that actually make their work and their lives better.

Our vision
==========

We work to achieve a world in which a mature portfolio of open source products and communities collaborate to support international development in highly useful, efficient ways. A reality where barriers are broken down by empowering individuals & local communities to share knowledge and tools. Where support and funding of technology-driven efforts in international development are coordinated within and between sectors.

We dream of a time when innovation is shared, and welcomed as a force of positive change to embrace, rather than a strategic organizational risk ... and an age when educated, empowered, and enabled individuals work to grow the numbers of citizens participating in digital society.

*************************
Our Goals for Communities
*************************

We believe in the importance of focusing on helping open source digital development projects increase their impact through four key pillars of maturity:

1. **Community effectiveness:** Inclusive & flexible governance models, distributed leadership, integrated community management processes.
2. **Product development:** Integrated product strategy and roadmap, behavior-based measurement of customer success.
3. **Robust technology architecture:** Collaborative & distributed technology leadership, use of appropriate tools for collaboration & software development, integrated formal & user-generated content.
4. **Sustainable organizational base:** Neutral home for vibrant multilateral participation, activist culture focused on stakeholder success, inclusive legal & IP policies that balance autonomy and collaboration.

******************
Guiding Principles
******************

Governance Principles
=====================

Many free & open source software projects align with the ideals of “meritocracy”, but the traditional implementation of meritocracy as practiced in other communities often disadvantages certain voices. Rather, the community strives toward a world where technology leads to more equity in the world, and is creating an environment where all voices are heard and encouraged to collaboratively build the best solutions.

As a “holarchy”, the community has a unique process of leadership & decision making. Organized in projects teams, each group is charged to make its own decisions about its software product or initiative; however, decisions are not made “in a vacuum." Each sub-group will need to consider:

* Pursuing additional co-creators
* Finding ways for newcomers to participate.
* Coordinating with other projects and teams
* Building strategies on things like documentation, outreach, or diversity & inclusion.

Groups may self-organize and work their way through the community membership and service application process to ensure their viability.

Finally, we are a community of individuals.  Individuals contribute through writing code, reviewing code, creating bugs, writing documentation, designing graphics, creating and running tests, etc. The community understands that some individuals may be doing this as part of their employment or other arrangement with a third-party organization. Organizations are strongly encouraged to allow individuals to offer their contributions on their own behalf and in their own name. Alternatively, an organization may submit (license) code (or other intellectual property assets) to the community and its sub-projects, however the organization itself can not be a contributor. Organizations will be recognized as sponsors and/or partners of the OSC. The individuals facilitating that partnership will be recognized as contributors.

Licensing Principles
====================

The following discussion is based around legal issues, but is not legal advice and is not written by a lawyer or legal professional.

A significant factor in license strategy will be based on whether or not products from OSC sub-projects will be combined into larger works. If so, license compatibility is a significant issue. Generally, it is very difficult to combine licenses due to incompatible restrictions that they often have about derivative works, distribution, etc.

**It is the recommendation of the OSC to utilize strong copyleft licenses, such as the GPL or AGPL for all community projects.** The most important factor applied to this decision was that of the social mission of the organization. Because OSC has expressly chartered its community by stating its value that access to technology is key to development of communities and individuals around the world, many funders and/or contributors within the community may feel that their contributions are being “exploited” to some extent by those who would adapt the open source project and improve upon it for profit without also contributing back those changes to improve the upstream product. (Indeed, this risk may be a tax liability for certain types of non-profit organizations, and in certain jurisdictions.) Further, because of the service-based nature of many OSC projects, the AGPL is specifically recommended as it requires those who would make improvements upon the code (such as an implementing organization funding developers) to offer those changes back at the point where they “distribute” their derivative work by deploying it to a server. (Otherwise, such organizations would not necessarily have to distribute their changed source code if it is confined within their organization.) It is also noteworthy that some licenses, such as the Mozilla Public License (MPL), allow its code to be combined with and further licensed with GPL-licensed code, as long as the larger work is licensed under GPL.

In order to change the license of an open source project, it is typically necessary to get the permission of all copyright holders. Unless the project was operating under a Copyright Assignment Agreement (CAA) or some types of Copyright License Agreement (CLA) for contributors, this means you would have to go back to the project’s past contributors and get them to agree to the re-licensing. This effort is likely to take significant time and energy, and may prove unsuccessful. For this reason, moving multiple OSC projects into an organization like Apache Software Foundation (ASF) is unlikely to be successful because of an organization-wide requirement to use the Apache Public License (APL). **For mentored small projects with only a few committers, however, it is worth attempting to relicense the code by getting permission from those copyright owners.**

From an ease-of-implementation perspective, the path of least resistance is to host projects in an organization that allows any license approved by both the OSI and the FSF, who maintain the canonical “approved license” lists for the industry. However, the burden in this approach is mostly placed upon people who would like to contribute to multiple sub-projects within the community -- they will need to be much more vigilant about checking licenses for each project and ensuring they commit their code with the proper license. Using multiple licenses is liable to create confusion for all but the most experienced contributors; projects should ensure they do not merge code with an improper license.

***In summary, for mentored projects, OSC should begin by officially embracing the copyleft GPL & AGPL licenses, officially encourage their use as a default, and then accept other free & open source licenses on a case by case basis. During a project’s mentorship process, it should be evaluated on the practicality of switching to these licenses, either on a short term or longer term timeline. Should that not be possible or reasonable, other licenses can be considered, with preference first toward weak copyleft licenses, and then other permissive licenses.***

Finance Principles
==================

Historically, many OSC projects have grown based on responses to needs expressed by organizations and entities funding international
development initiatives. These funders may have spearheaded or co-facilitated the development of an entire software project, or they may have sponsored the development of new or improved features in software. Often, this funding has been based on time-limited and scope-limited grants.

**We encourage sub-projects to seek potential funding opportunities** for development and advancement of their specific software products. Through its partner Software Freedom Conservancy, OSC offers a non-profit fiscal home for management of those funds, which can apply such resources as funding or reimbursement for individual contributors and volunteers who perform specific tasks. **Such decisions will be made by the Project Leader (in consultation with their entire group) along with the OSC Governance Advisory Board.** The Technical Steering Committee exists to assist & coordinate prioritization of feature development or technical efforts of each sub-project.

Unfortunately, a business model in which most resources are dedicated toward growth is rarely a sustainable one. A mature FOSS project -- especially one of a larger size -- requires effort in traditional “engineering management” tasks such as product & project management, maintenance of a feature development backlog, performance engineering, release management, support, and maintenance of bugs as they are discovered. While FOSS development can often be done by volunteers, history has shown that these efforts alone are not enough to maintain these critical needs. Therefore, additional consideration should be paid to sustainability and resourcing the project as a whole.

As such, the community will **additionally seek separate core funding** for initiatives such as management, maintenance of projects, events, and infrastructure. **A to-be-determined percentage of all project-specific funding will be earmarked for these types of activities,** to prevent investment of all community resources on new features at the neglect of maintaining the high level of software quality our customers expect.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
    
