.. _partnership-strategy:

########################
OSC Partnership Strategy
########################

The Open Source Center is designed as a long-term multilateral partnership that brings many experts & organizations together in service of  software development projects in the T4D space. Our aim is to be a "big tent" that will draw projects in and help direct them to various kinds of valuable resources. 

**********************************
Founding & Sustainability Partners
**********************************

As a founder, DIAL is bringing several key resources to the table for participating member projects, initially:

1. DIAL is providing **financial support** to participating member projects through regular offerings for **small-scale "catalytic grants"** that are tactically focused on narrow concerns like user experience, documentation, or software maintainability. DIAL also provides less frequent **larger strategic grants,** such as assisting projects to become independent community-driven entities after periods of single-organization ownership.

2. DIAL provides **direct technical assistance through its own staff** on topics related to software engineering management, community development/management, or project fiscal/organizational sustainability.

3. DIAL facilitates and currently funds **technical assistance provided through third-party service delivery partners.** Examples include items such as IT infrastructure providers, software-as-a-service collaboration tools, documentation writers, UX/UI designers, and legal consulting services.

*************************
Service Delivery Partners
*************************

More detailed information about our existing and potential service delivery partners is forthcoming.

*************************
Domain-Expertise Partners
*************************

A different type of service delivery partner for the Open Source Center are **domain-specific expert organizations who help in leading the OSC's domain-specific working groups,** such as Digital Square’s role in the health sector. These working groups are designed to serve as a kind of focal point for open source software projects working in a specific domain to collaborate closer with each other and stay informed about trends, funding sources/opportunities, and increase partnership or collaboration between projects within the domain.

With their expertise in the global health domain, the OSC asked Digital Square to start in this role for our health sector group. As we ramp up our program, the intent is for the this group, organized by colleagues at Digital Square, to facilitate communication & meetings amongst our participating member projects in the health field. **Importantly, this work is separate and distinct from any other funding opportunities that Digital Square may host.** In its “secretariat” role, Digital Square will work to raise the group’s awareness of funding opportunities from all sources that are targeted at the health sector. 

Through its role on the OSC Governance Advisory Board, **the Digital Square team also assists DIAL in assessment of health domain applicants for OSC grants.** For example, during a recent small catalytic grant offering, Digital Square provided the grant review panel guidance on prioritization of a single project’s three proposals for areas of investment. Additionally, Digital Square forwards the OSC Board guidance and potential leads for strategic grant opportunities when they are approached with funding proposals that do not meet their charter or scope. In their current round of funding, for example, OSC has received a short referral list of potential investments for open source community development that Digital Square would not be able to fund.

**The OSC intends to launch multiple sector-specific focus groups,** and is currently in discussion with a well-known expert non-profit organization to serve in a similar role for the fintech sector, with more to come as the OSC program grows.
